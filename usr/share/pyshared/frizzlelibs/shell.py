import os

def execute(opts):

# variabloj

    ARG=opts['destination'] + '/' + opts['echo']
    SHARED=opts['shared']

# catch duplicated project names
    n = 0
    
    with open(opts['config'], 'r') as skeleto:
        for line in skeleto:
            if n == 0:
                if os.path.exists(ARG):
                    try:
                        curlist = os.listdir(opts['destination'])
                        matching = [s for s in curlist if opts['echo'] + "_" in s]
                        sortlist = []
                        for item in matching:
                            sortlist.append(int(item.split("_")[1]))
                        n = max(sortlist) +1
                        ARG = ARG + '_' + str(n)
                    except: 
                        n = n + 1
                        ARG = ARG + '_' + str(n)
                else:
                    n = 1

# create the directory tree
                        
            if line.split(':')[0] == 'mkdir':
                target = ARG + '/' + line.split(':')[1]
                os.makedirs(target.rstrip())
            elif line.split(':')[0] == 'copyfile':
                shutil.copyfile(line.split(':')[1], line.split(':')[2])
            else:
                target = line.split(':')[1]
                source = ARG + '/' + line.split(':')[2]
                os.symlink(target.replace('%ARTKIT%', SHARED), source.rstrip() )
