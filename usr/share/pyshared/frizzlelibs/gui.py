import sys
import os
import shutil
import argparse
import shell
import dbus
import sqlite3 as sq

try:    
    from PySide.QtCore import *
    from PySide.QtGui import *
except:
    from PyQt4.QtCore import *
    from PyQt4.QtGui import *

class Fenestro(QMainWindow):
    def __init__(self,opts,parent=None):
        super(Fenestro, self).__init__(parent)
        self.central = QWidget()
        self.fname = opts['echo']
        self.setCentralWidget(self.central)
        gridbox = QGridLayout(self.central)
        '''
        cssfile="etc/frizzle/style.css"

        with open(cssfile,"r") as css:
            self.setStyleSheet(css.read())
        '''
        self.setWindowTitle("The Frizzle Database")

        if not os.path.isfile(opts['echo']):
            #print(self.fname + " database file found. Loading...")
            print('Database file not found, making a new one.')
            con = sq.connect(self.fname)
            with con:
                cur = con.cursor()
                cur.execute("CREATE TABLE Students(Id INTEGER PRIMARY KEY, Last TEXT, First TEXT, Birth TEXT, Parent TEXT, Emergency TEXT, Email TEXT, Medical TEXT, Photo INT, Notes TEXT, Start TEXT, Recent TEXT)")
                cur.execute("CREATE TABLE Classes(Id INTEGER PRIMARY KEY, Course TEXT, Age TEXT)" )
                cur.execute("CREATE TABLE Checker(Id INTEGER PRIMARY KEY, Taken INT)" )

                # dummy data
                cur.execute("INSERT INTO Students VALUES(0,'Penguin','Rockhopper', '92-01-01', 'Geeks and Nerds', '555-2323', 'foo@bar.com', 'Bee stings', 1, 'Here are notes', '2015-01-01', '2015-08-08')")
                cur.execute("INSERT INTO Students VALUES(1,'Penguin','Gentoo', '94-02-04', 'Foo and Bar', '523-4242', 'example@example.com', 'No medical issues', 0, 'Note there is no photo release', '2014-02-09', '2015-03-03')")
                cur.execute("INSERT INTO Students VALUES(2,'Penguin','Blue', '97-01-04', 'New Zealand', '4545-4242', 'blue@aotereoa.co.nz', 'Small', 1, 'Photo release back on', '2014-03-04', '2015-05-06')")
            con.close()

        self.con = sq.connect(self.fname, isolation_level=None)

        #fields
        self.cur = self.con.cursor()
        self.cur.execute('SELECT * FROM Students;')
        self.recsq = self.cur.fetchall()

        self.idl = QLabel()
        self.namel = QLabel("Student Name")

        self.lastf = QLineEdit()
        self.firstf = QLineEdit()
        self.lastf.selectAll()

        self.birthl = QLabel("Date of Birth")
        self.birthf = QLineEdit()

        self.parentl = QLabel("Parent Names")
        self.parentf = QLineEdit()

        self.emergl = QLabel("Emergency Contact")
        self.emergf = QLineEdit()

        self.emaill = QLabel("Email")
        self.emailf = QLineEdit()

        self.medicl = QLabel("Medical Information")
        self.medicf = QTextEdit()

        self.photol = QLabel("Photo Release")
        self.photof = QCheckBox()

        self.notel = QLabel("Makerbox Notes")
        self.notef = QTextEdit()

        self.startl = QLabel("First Attended")
        self.startf = QLineEdit()

        self.recentl = QLabel("Most Recent")
        self.recentf = QLineEdit()

        #self.recdict = {'Id':'idl', 'Last':'lastf', 'First':'firstf', 'Birth':'birthf', 'Parent':'parentf', 'Emergency':'emergf', 'Email':'emailf', 'Medical':'medicf', 'Photo':'photof', 'Notes':'notef', 'Start':'startf', 'Recent':'recentf'}

        #layout
        gridbox.addWidget(self.namel, 0,0)
        gridbox.addWidget(self.lastf, 0,1)
        gridbox.addWidget(self.firstf, 0,2)

        gridbox.addWidget(self.birthl, 2,0)
        gridbox.addWidget(self.birthf, 2,1)

        gridbox.addWidget(self.parentl, 3,0)
        gridbox.addWidget(self.parentf, 3,1)

        gridbox.addWidget(self.emergl, 4,0)
        gridbox.addWidget(self.emergf, 4,1)

        gridbox.addWidget(self.emaill, 5,0)
        gridbox.addWidget(self.emailf, 5,1)

        gridbox.addWidget(self.medicl, 6,0)
        gridbox.addWidget(self.medicf, 6,1)

        gridbox.addWidget(self.photol, 7,0)
        gridbox.addWidget(self.photof, 7,1)

        gridbox.addWidget(self.notel, 8,0)
        gridbox.addWidget(self.notef, 8,1)

        gridbox.addWidget(self.startl, 9,0)
        gridbox.addWidget(self.startf, 9,1)
        gridbox.addWidget(self.recentl, 9,2)
        gridbox.addWidget(self.recentf, 9,3)

        gridbox.addWidget(self.idl, 10,0)

        self.setGeometry(200,200,450,250)
        self.preferoj = {}

        #initial population
        self.idsq = 0
        self.Populate(self.recsq,self.idsq)
        self.Main(self.recsq,self.idsq)


    def Main(self,recsq,idsq):
        #print('recsq Main @ ' + str(self.recsq)) #debug
        #create statusbar
        self.statusBar().showMessage(self.fname + ' Loaded')

        #create toolbar
        exitAction = QAction(QIcon('/usr/share/icons/oxygen/22x22/actions/application-exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(qApp.quit)        
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)

        previousAction = QAction(QIcon('/usr/share/icons/oxygen/22x22/actions/go-previous.png'), 'Previous', self)
        previousAction.setShortcut('Ctrl+P')
        previousAction.triggered.connect(self.Pbtn)
        #self.toolbar = self.addToolBar('Next')
        self.toolbar.addAction(previousAction)

        nextAction = QAction(QIcon('/usr/share/icons/oxygen/22x22/actions/go-next.png'), 'Next', self)
        nextAction.setShortcut('Ctrl+N')
        nextAction.triggered.connect(self.Nbtn)
        #self.toolbar = self.addToolBar('Next')
        self.toolbar.addAction(nextAction)

        saveAction = QAction(QIcon('/usr/share/icons/oxygen/22x22/actions/document-save.png'), 'Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.triggered.connect(self.Sbtn)
        #self.toolbar = self.addToolBar('Save')
        self.toolbar.addAction(saveAction)

        #self.pbtn.clicked.connect(self.Pbtn)
        #self.nbtn.clicked.connect(self.Nbtn)
        #self.sbtn.clicked.connect(self.Sbtn)
        

    def Populate(self,recsq,idsq):
        #print('idsq Populate @ ' + str(idsq)) #debug
        self.recentf.setText(str(recsq[idsq][11]))
        self.startf.setText(str(recsq[idsq][10]))
        self.notef.setText(str(recsq[idsq][9]))
        self.photof.setChecked(int(recsq[idsq][8]))
        self.medicf.setText(str(recsq[idsq][7]))
        self.emailf.setText(str(recsq[idsq][6]))
        self.emergf.setText(str(recsq[idsq][5]))
        self.parentf.setText(str(recsq[idsq][4]))
        self.birthf.setText(str(recsq[idsq][3]))
        self.firstf.setText(str(recsq[idsq][2]))
        self.lastf.setText(str(recsq[idsq][1]))
        self.statusBar().showMessage(str(recsq[idsq][0]))
        #self.idl.setText(str(recsq[idsq][0]))
        
    def Pbtn(self,recsq):
        #print('self.recsq Nbtn @ ' + str(self.recsq)) #debug
        print('idsq Nbtn @ ' + str(self.idsq)) #debug
        self.idsq -= 1
        self.Populate(self.recsq,self.idsq)

    def Nbtn(self):
        #print('self.recsq Nbtn @ ' + str(self.recsq)) #debug
        print('idsq Nbtn @ ' + str(self.idsq)) #debug
        self.idsq += 1
        self.Populate(self.recsq,self.idsq)

    def Sbtn(self):
        nudata = str(self.lastf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Last", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.firstf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="First", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.birthf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Birth", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.parentf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Parent", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.emergf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Emergency", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.emailf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Email", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.medicf.toPlainText())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Medical", np=nudata, idf="Id", idsq=self.idsq))

        nudata = int(self.photof.isChecked())
        print(nudata)
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Photo", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.notef.toPlainText())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Notes", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.startf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Start", np=nudata, idf="Id", idsq=self.idsq))

        nudata = str(self.recentf.text())
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Recent", np=nudata, idf="Id", idsq=self.idsq))

        # somewhere in here lies the right way to do this
        #for key,value in self.recdict.iteritems():
            #print(key + " -> " + value)            

        self.con.commit()

        self.cur.execute("SELECT * FROM {tn} WHERE {idf}=('{idsq}')".format(tn="Students", idf="Id", idsq=self.idsq))

        for row in self.cur.fetchall():
            print(row)
            chango = str(row[4])
        #load changes...well, everything..
        self.cur.execute('SELECT * FROM Students;')
        self.recsq = self.cur.fetchall()
        self.Populate(self.recsq,self.idsq)


        '''
        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Parent", np=NUPARENT, idf="Id", idsq=self.idsq))

        self.cur.execute("UPDATE {tn} SET {cn}=('{np}') WHERE {idf}=('{idsq}')".format(tn="Students", cn="Parent", np=NUPARENT, idf="Id", idsq=self.idsq))
        '''



def execute(opts):
    frizzlegui = QApplication(sys.argv)
    window = Fenestro(opts)
    window.show()
    frizzlegui.exec_()
